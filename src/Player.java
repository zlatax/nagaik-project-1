import bagel.*;

public class Player extends Entity{
    private int energy;  //displayed at position (20,760) in black font size 20 and font DejaVuSans-Bol(ttf file provided in res zip file)
    private final static int stepsize = 10;

    public Player(String filename, APoint pos, int energy) {
        super(filename, pos);
        this.energy = energy;
    }

    public int getEnergy() {
        return energy;
    }

    public int getStepsize() {
        return stepsize;
    }

    public void addEnergy(int energy) {
        this.energy += energy;
    }
}
