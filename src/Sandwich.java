import bagel.*;

/**
 * Sandwich class in ShadowTreasure game,
 * which will increase player's energy when passed through.
 */
public class Sandwich extends Entity {
    public Sandwich(String filename, APoint pos) {
        super(filename, pos);
    }
}
