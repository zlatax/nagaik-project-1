import java.lang.*;

/**
 * Zombie class in ShadowTreasure game,
 */
public class Zombie extends Entity{

    public Zombie(String filename, APoint pos) {
        super(filename, pos);
    }
}
