import java.lang.Math;

/**
 * APoint class in ShadowTreasure game.
 * contains X and Y coordinates for location management in ShadowTreasure game.
 */
public class APoint {
    private double x,y;

    public APoint(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public double distanceTo(APoint other) {
        return Math.sqrt(Math.pow(this.x - other.getX(),2) + Math.pow((this.y - other.getY()) ,2));
    }

    public APoint getDirection (APoint other) {
        double xd = (other.x-this.x)/distanceTo(other);
        double yd = (other.y-this.getY())/distanceTo(other);

        return new APoint(xd,yd);
    }

    @Override
    public String toString() {
        return "X: "+this.x+"\tY: "+this.y;
    }
}
