/**
    Shadow Treasure Game
 Template Code by The University of Melbourne
 Code edited by: Keigo Nagai (1112132)
 */

import bagel.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.text.DecimalFormat;
import java.util.Scanner;


public class ShadowTreasure extends AbstractGame {

    public static final String UTF8_BOM = "\uFEFF";

    // for rounding double number; use this to print the location of the player
    private static DecimalFormat df = new DecimalFormat("0.00");

    private Player player;
    private Zombie zombie;
    private Sandwich sandwich;
    private int count=0;
    private final Font deja = new Font("res/font/DejaVuSans-Bold.ttf",24);
    private Image bg;

    public static void printInfo(double x, double y, int e) {
        System.out.println(df.format(x) + "," + df.format(y) + "," + e);
    }
    
    public ShadowTreasure() throws IOException {
        super(1024, 768, "ShadowTreasure");
        this.loadEnvironment("test/test2/environment.csv");
        // Add code to initialize other attributes as needed
        bg = new Image("res/images/background.png");
    }

    /**
     * Load from input file
     */
    private void loadEnvironment(String filename) {
        // Code here to read from the file and set up the environment
        try {
            Scanner scanner = new Scanner(new File(filename), StandardCharsets.UTF_8);
            double xcoor, ycoor;
            int level, counter=0;
            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                if(counter==0 & line.startsWith(UTF8_BOM)) {
                    line = line.substring(1); //remove BOM char
                    counter++;
                }
                Scanner linescan  = new Scanner(line);
                linescan.useDelimiter(",");
                String item = linescan.next();

                switch (item.toLowerCase()) {
                    case "player" -> {
                        xcoor = Double.parseDouble(linescan.next());
                        ycoor = Double.parseDouble(linescan.next());
                        level = Integer.parseInt(linescan.next());
                        player = new Player("res/images/player.png", new APoint(xcoor, ycoor), level);
                    }
                    case "zombie" -> {
                        xcoor = Double.parseDouble(linescan.next());
                        ycoor = Double.parseDouble(linescan.next());
                        zombie = new Zombie("res/Images/Zombie.png", new APoint(xcoor, ycoor));
                    }
                    case "sandwich" -> {
                        xcoor = Double.parseDouble(linescan.next());
                        ycoor = Double.parseDouble(linescan.next());
                        sandwich = new Sandwich("res/Images/Sandwich.png", new APoint(xcoor, ycoor));
                    }
                    default -> throw new IllegalStateException("Unexpected value: " + item.toLowerCase());
                }
                linescan.close();
            }
            scanner.close();

        } catch(FileNotFoundException e) {
            System.out.println("File was not found!\n"+e);
        } catch(Exception e) {
            System.out.println("There is an unexpected error! "+e+"\n"+e.getStackTrace()[0].getLineNumber());
        }
    }

    /**
     * Performs a state update.
     */
    @Override
    public void update(Input input) {
        bg.drawFromTopLeft(0,0);
        deja.drawString(String.format("energy: %d",player.getEnergy()),20,760);

        if(count==10) {
            APoint playerPos = player.getPos();
            APoint zombiePos = zombie.getPos();

            printInfo(playerPos.getX(),playerPos.getY(),player.getEnergy());

            // Logic to update the game, as per specification must go here
            if (playerPos.distanceTo(zombiePos) < 50) {
                player.addEnergy(-3);
                APoint dir = playerPos.getDirection(zombiePos);
                playerPos.setX(playerPos.getX() + dir.getX() * player.getStepsize());
                playerPos.setY(playerPos.getY() + dir.getY() * player.getStepsize());
                printInfo(playerPos.getX(),playerPos.getY(),player.getEnergy());
                System.exit(0);
            } else if (sandwich != null && playerPos.distanceTo(sandwich.getPos()) < 50) {
                player.addEnergy(5);
                sandwich = null;
            }  // after user reaches sandwich, the object does ot exist therefore we need to rethink ocndiiton
            if (player.getEnergy() >= 3) {
                APoint dir = playerPos.getDirection(zombiePos);
                playerPos.setX(playerPos.getX() + dir.getX() * player.getStepsize());
                playerPos.setY(playerPos.getY() + dir.getY() * player.getStepsize());
            } else {
                APoint dir = playerPos.getDirection(sandwich.getPos());
                playerPos.setX(playerPos.getX() + dir.getX() * player.getStepsize());
                playerPos.setY(playerPos.getY() + dir.getY() * player.getStepsize());
            }

            count=0;
        }
        count++;

        // Keyboard input
        if(input.wasPressed(Keys.ESCAPE)) {
            Window.close();
        }

        // constant info on window

        player.getImg().draw(player.getPos().getX(),player.getPos().getY());
        zombie.getImg().draw(zombie.getPos().getX(),zombie.getPos().getY());
        if(sandwich!=null) {
            sandwich.getImg().draw(sandwich.getPos().getX(),sandwich.getPos().getY());
        }

    }

    // Removes
    private static String removeUTF8BOM(String string) {
        if (string.startsWith(UTF8_BOM)) {
            string = string.substring(1);
        }
        return string;
    }


    /**
     * The entry point for the program.
     */
    public static void main(String[] args) throws IOException {
        ShadowTreasure game = new ShadowTreasure();
        game.run();
    }
}
