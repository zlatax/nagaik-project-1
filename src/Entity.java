import bagel.*;

import bagel.util.*;
import java.lang.*;

/**
 * Entity abstract class in ShadowTreasure game,
 * This class provides a frame for all objects which exist in
 * the ShadowTreasure game.
 */
public abstract class Entity {
    private Image img;
    private APoint pos;

    public Entity(String filename, APoint pos) {
        this.img = new Image(filename);
        this.pos = pos;
    }

    public Image getImg() { return img; }

    public APoint getPos() {
        return pos;
    }

    public void setPos(APoint pos) { this.pos = pos; }

}
